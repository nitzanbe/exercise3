<?php
    class Htmalpage{
        protected $title = 'title';
        protected $body = 'body';
        public function view(){
            echo "
                <html>
                <head>
                    <title>$this->title</title>
                </head>
                <body>
                    $this->body
                </body>
                </html>
                ";
        }
        function __construct($title = "",$body = ""){
            $this->title = $title;            
            $this->body = $body;
        }
    }

    class coloredMessage extends Htmalpage {
        protected $color;
        public function __set($property,$value){
            if ($property == 'color'){
                $colors = array('red','pink','blue');
                if(in_array($value,$colors)){
                    $this->color = $value; 
                } else{
                    die("The color does not exist");
                }
            }
        }
        public function view(){
            echo "
            <html>
            <head>
                <title>$this->title</title>
            </head>
            <body>
                <p style = 'color:$this->color'  >$this->body</p>
            </body>
            </html>
            ";
        }
    }

    class sizeMessage extends coloredMessage {
        protected $size;
        public function __set($property,$value){
            if ($property == 'size'){
                $s=$value-($value%100);
                if($value>=10&&$value<=24&&$s==0){
                    $this->size = $value; 
                }else{
                    die("The size is invalid");
                }
            }elseif ($property == 'color') {
                parent::__set($property,$value);
            }
        }
        public function view(){
            echo "
            <html>
            <head>
                <title>$this->title</title>
            </head>
            <body> 
                <p style = 'color:$this->color; font-size:$this->size;' >$this->body</p>
            </body>
            </html>
            ";
        }
    }
?>











